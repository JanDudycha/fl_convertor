from Farm import Farm
import math


def count_unit(unit_position, unit_name, line):
    pom = unit_position + len(unit_name)
    unit_count = ""

    while line[pom] != ' ':
        unit_count += line[pom]
        pom += 1
        if pom + 1 == len(line):
            break

    if unit_count == "":
        return 0
    else:
        return unit_count


def find_name(i, input_data):
    last_line = input_data[i - 1]
    name = ""

    for a in range(30):
        if a > 3 and last_line[a] == "\t":
            break

        if last_line[a] != "\t":
            name += last_line[a]

    return name


def load_farms_from_database(farm_database):
    farms = []
    position = 0
    for i in farm_database:
        farm_name = ""
        unit_name = ""
        unit_count = ""
        current_bounty = ""
        bounty = []
        for j in i:
            if j == "_":
                if position > 2:
                    bounty.append(int(current_bounty))
                    current_bounty = ""
                position += 1
            elif j == "\n":
                bounty.append(int(current_bounty))
                position = 0
                break
            else:
                if position == 0:
                    farm_name += j
                elif position == 1:
                    unit_name += j
                elif position == 2:
                    unit_count += j
                if position > 2:
                    current_bounty += j

        farms.append(Farm(farm_name, unit_name, int(unit_count), bounty))

    return farms


def recognize_unit(i, input_data):
    last_line = input_data[i - 1]

    merc_position = last_line.find("Mercenary")
    bow_position = last_line.find("Bowman")
    step_position = last_line.find("Steppe Rider")
    marauder_position = last_line.find("Marauder")

    club_position = last_line.find("Clubswinger")
    paladin_position = last_line.find("Paladin")
    teutonic_position = last_line.find("Teutonic Knight")

    legionnaire_position = last_line.find("Legionnaire")
    imperian_position = last_line.find("Imperian")
    imperatoris_position = last_line.find("Equites Imperatoris")
    caesaris_position = last_line.find("Equites Caesaris")

    if merc_position > -1:
        return [int(count_unit(merc_position, "Mercenary", last_line)), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    elif bow_position > -1:
        return [0, int(count_unit(bow_position, "Bowman", last_line)), 0, 0, 0, 0, 0, 0, 0, 0, 0]
    elif step_position > -1:
        return [0, 0, int(count_unit(step_position, "Steppe Rider", last_line)), 0, 0, 0, 0, 0, 0, 0, 0]
    elif marauder_position > -1:
        return [0, 0, 0, int(count_unit(marauder_position, "Marauder", last_line)), 0, 0, 0, 0, 0, 0, 0]
    elif club_position > -1:
        return [0, 0, 0, 0, int(count_unit(club_position, "Clubswinger", last_line)), 0, 0, 0, 0, 0, 0]
    elif paladin_position > -1:
        return [0, 0, 0, 0, 0, int(count_unit(paladin_position, "Paladin", last_line)), 0, 0, 0, 0, 0]
    elif teutonic_position > -1:
        return [0, 0, 0, 0, 0, 0, int(count_unit(teutonic_position, "Teutonic Knight", last_line)), 0, 0, 0, 0]
    elif legionnaire_position > -1:
        return [0, 0, 0, 0, 0, 0, 0, int(count_unit(legionnaire_position, "Legionnaire", last_line)), 0, 0, 0]
    elif imperian_position > -1:
        return [0, 0, 0, 0, 0, 0, 0, 0, int(count_unit(imperian_position, "Imperian", last_line)), 0, 0]
    elif imperatoris_position > -1:
        return [0, 0, 0, 0, 0, 0, 0, 0, 0, int(count_unit(imperatoris_position, "Equites Imperatoris", last_line)), 0]
    elif caesaris_position > -1:
        return [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, int(count_unit(caesaris_position, "Equites Caesaris", last_line))]
    else:
        return [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]


def decode_name(unit_count_data):
    if unit_count_data[0] > 0:
        return "Mercenary"
    elif unit_count_data[1] > 0:
        return "Bowman"
    elif unit_count_data[2] > 0:
        return "Steppe Rider"
    elif unit_count_data[3] > 0:
        return "Marauder"
    elif unit_count_data[4] > 0:
        return "Clubswinger"
    elif unit_count_data[5] > 0:
        return "Paladin"
    elif unit_count_data[6] > 0:
        return "Teutonic Knight"
    elif unit_count_data[7] > 0:
        return "Legionnaire"
    elif unit_count_data[8] > 0:
        return "Imperian"
    elif unit_count_data[9] > 0:
        return "Equites Imperatoris"
    elif unit_count_data[10] > 0:
        return "Equites Caesaris"
    return None


def decode_unit_count(unit_count_data):
    for i in unit_count_data:
        if i > 0:
            return i
    return None


def sort_by_difference(e):
    return e.unit_difference


def sort_by_avg_bounty(e):
    return e.avg_bounty


def print_units(units):
    temp_string = ""
    if units[0] > 0 or units[1] > 0 or units[2] > 0 or units[3] > 0:
        temp_string = "| Mercenary . . . : " + str(units[0]) + "\n| Bowman. . . . . : " + str(
            units[1]) + "\n| Steppe Rider. . : " + str(units[2]) + "\n| Marauder. . . . : " + str(units[3])
    elif units[4] > 0 or units[5] > 0 or units[6] > 0:
        temp_string = "| Clubswinger. . . . : " + str(units[4]) + "\n| Paladin. . . . . . : " + str(
            units[5]) + "\n| Teutonic Knight. . : " + str(units[6])
    elif units[7] > 0 or units[8] > 0 or units[9] > 0 or units[10] > 0:
        temp_string = "| Legionnaire. . . . . . : " + str(units[7]) + "\n| Imperian . . . . . . . : " + str(
            units[8]) + "\n| Equites Imperatoris. . : " + str(units[9]) + "\n| Equites Caesaris . . . : " + str(
            units[10])

    return temp_string


def count_carry_capacity(units):
    if units[0] > 0 or units[1] > 0 or units[2] > 0 or units[3] > 0:
        return units[0] * 50 + units[1] * 30 + units[2] * 75 + units[3] * 80
    elif units[4] > 0 or units[5] > 0 or units[6] > 0:
        return units[4] * 60 + units[5] * 110 + units[6] * 80
    elif units[7] > 0 or units[8] > 0 or units[9] > 0 or units[10] > 0:
        return units[7] * 50 + units[8] * 50 + units[9] * 100 + units[10] * 70
    return None


def get_unit_capacity(name_of_unit):
    if name_of_unit == "Mercenary":
        return 50
    elif name_of_unit == "Bowman":
        return 30
    elif name_of_unit == "Steppe Rider":
        return 75
    elif name_of_unit == "Marauder":
        return 80
    elif name_of_unit == "Clubswinger":
        return 60
    elif name_of_unit == "Paladin":
        return 110
    elif name_of_unit == "Teutonic Knight":
        return 80
    elif name_of_unit == "Legionnaire":
        return 50
    elif name_of_unit == "Imperian":
        return 50
    elif name_of_unit == "Equites Imperatoris":
        return 100
    elif name_of_unit == "Equites Caesaris":
        return 70
    else:
        print("CHYBA funkce get_unit_capacity(name_of_unit)")


def write_into_database(list_of_farms):
    temp = ""
    for i in list_of_farms:
        temp_bounty = "_".join(map(str, i.bounty))
        temp += i.name + "_" + i.name_of_farm_unit + "_" + str(i.amt_of_farm_unit) + "_" + temp_bounty + "\n"
    return temp


def insert_into_list_of_farms(farm_name, unit_count_data, bounty, list_of_farms):
    unit_name = decode_name(unit_count_data)
    unit_count = decode_unit_count(unit_count_data)

    for i in list_of_farms:
        if i.name == farm_name and i.name_of_farm_unit == unit_name and i.amt_of_farm_unit == unit_count:
            i.bounty.append(bounty)
            return list_of_farms

    list_of_farms.append(Farm(farm_name, unit_name, unit_count, [bounty]))
    return list_of_farms


def count_fl_efficiency(total_carry_capacity, total_bounty):
    pom = total_carry_capacity - total_bounty
    pom2 = total_carry_capacity - pom
    if total_carry_capacity != 0:
        return float((pom2 / total_carry_capacity) * 100)
    return 0


def print_current_fl_data(total_carry_capacity, total_bounty, total_attacks, FL_efficiency):
    temp_string = "___________________________________\n" + "| Total carry  . . . : " + str(
        total_carry_capacity) + spaces("| Total carry  . . . : " + str(
        total_carry_capacity)) + "\n| Total bounty . . . : " + str(total_bounty) + "\n| Total attacks. . . : " + str(
        total_attacks) + "\n| Average bounty . . : " + str(
        total_bounty / total_attacks) + "\n| FL EFFICIENCY. . . : " + str(
        FL_efficiency.__floor__()) + "%" + "\n|----------------------------------"

    return temp_string


def spaces(txt):
    spaces = ""

    return spaces


def calculate_efficiency_on_farms(list_of_farms):
    CARRY_RESERVE = 20
    temp_bounty = 0
    for i in list_of_farms:
        for j in i.bounty:
            temp_bounty += j
        i.total_bounty = temp_bounty
        i.avg_bounty = (temp_bounty / len(i.bounty)).__floor__()
        i.carry_waste = (i.amt_of_farm_unit * get_unit_capacity(i.name_of_farm_unit) - i.avg_bounty).__floor__()
        i.ideal_carry = ((i.avg_bounty / 100) * CARRY_RESERVE + i.avg_bounty).__floor__()
        i.ideal_number_of_unit = math.ceil(i.ideal_carry / get_unit_capacity(i.name_of_farm_unit))
        i.unit_difference = i.ideal_number_of_unit - i.amt_of_farm_unit
        temp_bounty = 0

    return list_of_farms


def get_spaces(name, param):
    spaces = ""
    for i in range(param - len(name)):
        spaces += "."
    return spaces


def underfarming(list_of_farms):
    list_of_farms.sort(reverse=True, key=sort_by_difference)
    output_string = ""
    for i in list_of_farms:
        if i.unit_difference > 0 and i.amt_of_farm_unit > 2:
            output_string += i.name + get_spaces(i.name, 35) + "AVG:" + str(i.avg_bounty) + get_spaces(
                str(i.avg_bounty), 7) + get_spaces(str(i.ideal_number_of_unit), 7) + " " + str(
                i.amt_of_farm_unit) + " ---> " + str(str(i.ideal_number_of_unit)) + " " + get_spaces(
                str(i.amt_of_farm_unit), 5) + " " + i.name_of_farm_unit + "\n"

    return output_string


def overfarming(list_of_farms):
    list_of_farms.sort(reverse=False, key=sort_by_difference)
    output_string = ""
    for i in list_of_farms:
        if i.unit_difference < 0 and i.amt_of_farm_unit > 2 and i.ideal_number_of_unit > 2:
            output_string += i.name + get_spaces(i.name, 35) + "AVG:" + str(i.avg_bounty) + get_spaces(
                str(i.avg_bounty), 7) + get_spaces(str(i.ideal_number_of_unit), 7) + " " + str(
                i.amt_of_farm_unit) + " ---> " + str(str(i.ideal_number_of_unit)) + " " + get_spaces(
                str(i.amt_of_farm_unit), 5) + " " + i.name_of_farm_unit + "\n"

    return output_string


def trash_farms(list_of_farms):
    list_of_farms.sort(reverse=False, key=sort_by_avg_bounty)
    output_string = ""
    for i in list_of_farms:
        if i.avg_bounty <= 20:
            output_string += i.name + get_spaces(i.name, 35) + "AVG:" + str(i.avg_bounty) + get_spaces(
                str(i.avg_bounty), 7) + get_spaces(str(i.ideal_number_of_unit), 7) + " " + str(
                i.amt_of_farm_unit) + " ---> " + "0 " + get_spaces(
                str(i.amt_of_farm_unit), 5) + " " + i.name_of_farm_unit + "\n"

    return output_string
