from Farm import Farm
import Functions

txt_file_contetnt = open("input.txt", "r+", encoding="utf8")
txt_farm_database = open("farmDatabase.txt", "r+", encoding="utf8")
txt_output = open("output.txt", "r+", encoding="utf8")


input_data = txt_file_contetnt.readlines()
farm_database = txt_farm_database.readlines()

farm_name = ""
list_of_farms = []
total_attacks = 0
total_bounty = 0
total_carry_capacity = 0
FL_efficiency = 0
units = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

""" 
NAME, UNIT_NAME, AMT, [BOUNTY, . , . ]
"""

if len(farm_database) > 0:
    list_of_farms = Functions.load_farms_from_database(farm_database)

txt_farm_database.seek(0)
txt_farm_database.truncate(0)
txt_output.seek(0)
txt_output.truncate(0)
txt_file_contetnt.seek(0)
txt_file_contetnt.truncate(0)

for i in range(len(input_data)):
    current_line = input_data[i]
    bounty_position = current_line.find("Bounty:")

    if bounty_position > -1:
        farm_name = Functions.find_name(i, input_data)
        unit_count_data = Functions.recognize_unit(i, input_data)

        for j in range(len(units)):
            units[j] += unit_count_data[j]

        pom = bounty_position + 8
        current_bounty = ""

        while current_line[pom] != ' ':
            current_bounty += current_line[pom]
            pom += 1
        total_bounty += int(current_bounty)
        total_attacks += 1

        list_of_farms = Functions.insert_into_list_of_farms(farm_name, unit_count_data, int(current_bounty),
                                                            list_of_farms)

total_carry_capacity = Functions.count_carry_capacity(units)
FL_efficiency = Functions.count_fl_efficiency(total_carry_capacity, total_bounty)


list_of_farms = Functions.calculate_efficiency_on_farms(list_of_farms)

'''WRITING UNDERFARMS AND OVERFARMS'''
txt_output.write(
    Functions.print_current_fl_data(total_carry_capacity, total_bounty, total_attacks, FL_efficiency) + "\n")
txt_output.write(Functions.print_units(units) + "\n¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯\n")

txt_output.write("\n ===========  UNDERFARMIG  ========== \n")
txt_output.write(Functions.underfarming(list_of_farms) + "\n")
txt_output.write(" ===========  OVERFARMING  ========== \n")
txt_output.write(Functions.overfarming(list_of_farms) + "\n")
txt_output.write(" =======  TRASH FARM PLZ REMOVE  ======= avg bounty less than 20 \n")
txt_output.write(Functions.trash_farms(list_of_farms) + "\n")

txt_farm_database.write(Functions.write_into_database(list_of_farms))

print(Functions.print_current_fl_data(total_carry_capacity, total_bounty, total_attacks, FL_efficiency))
print(Functions.print_units(units))

txt_file_contetnt.close()
txt_farm_database.close()
txt_output.close()
