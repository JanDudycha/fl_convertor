class Farm:

    total_bounty = 0
    avg_bounty = 0
    carry_waste = 0
    ideal_carry = 0
    ideal_number_of_unit = 0
    unit_difference = 0

    def __init__(self, name, name_of_farm_unit, amt_of_farm_unit, bounty):
        self.name = name
        self.name_of_farm_unit = name_of_farm_unit
        self.amt_of_farm_unit = amt_of_farm_unit
        self.bounty = bounty
